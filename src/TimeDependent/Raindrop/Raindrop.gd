extends Node2D

var time_flow = 1.0
var started = false
    
func _on_time_flow_changed(new_time_flow):
    if started:
        time_flow = new_time_flow
        $AnimationPlayer.set_speed_scale(time_flow) 
    
func start(_time_flow) -> void:
    time_flow = _time_flow
    yield(get_tree().create_timer(4*randf()), "timeout")
    $AnimationPlayer.play("rain")
    started = true
