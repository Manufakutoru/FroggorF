extends Area2D

export (float) var leap_interval = 0.8
export (float) var leap_speed = 12
export (float) var leap_distance = 75

signal frog_hit
signal frog_finished
var leap_progress = 0.0
var state = 0
enum {INACTIVE, ACTIVE}

func _ready() -> void:
    $LeapTimer.set_wait_time(leap_interval)
    change_state(INACTIVE)
    $LeapTimer.start((0.5+0.5*randf())*leap_interval)
    $LeapTimer.set_wait_time(leap_interval)
    
func start(pos) -> void:
    position = pos
    change_state(ACTIVE)
        
func _process(delta) -> void:
    if state == ACTIVE:
        if leap_progress > 0:
            var leap = delta * leap_speed
            leap_progress -= leap
            if leap_progress < 0:
                leap += leap_progress
                leap_progress = 0.0
            position.y -= leap_distance * leap
                        
func change_state(new_state) -> void:
    match new_state:
        # collision off
        INACTIVE:
            $LeapTimer.set_paused(true)
            $CollisionShape2D.set_deferred("disabled", true)
        # collision on
        ACTIVE:
            $LeapTimer.set_paused(false)
            $CollisionShape2D.set_deferred("disabled", false)
    state = new_state
    
func _on_LeapTimer_timeout():
    leap_progress = 1.0
    $JumpPlayer.play("jump")

func _on_Frog_area_entered(area: Area2D):
    if state == ACTIVE:
        if area.is_in_group('obstacles'):
            _frog_hit()
        elif area.is_in_group('goals'):
            _frog_finished()

func _frog_hit():
    change_state(INACTIVE)
    emit_signal("frog_hit")
    call_deferred("queue_free")

func _frog_finished():
    change_state(INACTIVE)
    emit_signal("frog_finished")
    queue_free()
