extends Node

var highscores_file: = File.new()
var highscores_file_path: = "user://highscores.save"
var highscores: = {}
var level_nr = 1

func _ready() -> void:
    if not highscores_file.file_exists(highscores_file_path):
        write_highscores()
    else:
        read_highscores()

func write_highscores() -> void:
    highscores_file.open(highscores_file_path, File.WRITE)
    highscores_file.store_var(highscores)
    highscores_file.close()

func read_highscores() -> void:
    highscores_file.open(highscores_file_path, File.READ)
    highscores = highscores_file.get_var()
    highscores_file.close()
