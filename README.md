# FroggorF

Bring all frogs to the other side of the Road, by manipulating the time for their surroundings.

This was created for the 062021 Edition of [LibreJam](https://leagueh.xyz/en/librejam/index.html) with the theme "Time".

## How to Play

Use the slider at the bottom of the screen to manipulate the time for the vehicles on the road.

## Licensing

Code is under GPLv3.

Car sound is an altered version of a cc0 sound from [freesound](https://freesound.org/)
Rain sound is cc-by 3 by straget from [freesound](https://freesound.org/people/straget/sounds/531947/)

The assets created by us and under CC-BY-SA 4.0.

The font "Amaranth" used in this project is licensed under the OFL and
obtained from [Font Squirrel](https://www.fontsquirrel.com/fonts/amaranth).
